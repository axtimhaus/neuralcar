from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler
from gensim.models import word2vec
from gensim.models import KeyedVectors as kv
from pymongo import MongoClient
from bson.objectid import ObjectId
from datetime import datetime
from pytorch_pretrained_bert import BertTokenizer, BertModel, BertForMaskedLM
import classification
import torch
import logging
import pickle as pkl
import numpy as np
import pprint
import matplotlib.pyplot as plt
import traceback

pp = pprint.PrettyPrinter(indent=3)

client = MongoClient('mongodb://localhost:27017')
db = client["ncar_engine"]

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')

counter = 0
ranks = []

notes = ["with distance, bert", "without distance, bert", "with distance, w2v", "without distance, w2v"]

def load_embeddings():
    fname = "/proj/zimmermann/ncar/System/embeddings/glove.840B.300d.txt"
    print("Loading embeddings.", end="\r")
    try:
        model = kv.load(fname)
    except: 
        model = kv.load_word2vec_format(fname, binary=False)
    print("Done.              ")
    return model

save_ana = [None,None]

def add_embedding(x,model,antecedent_span, anaphora_span,text, oov=None, bert=False):
    
    if bert:
        
        if [anaphora_span, len(text)] == save_ana[0]:
            last_4_layers = save_ana[1][0]
            text_map = save_ana[1][1]
            
            try:
                idx1 = text_map[antecedent_span[0]][-1]
                idx2 = text_map[antecedent_span[1]][0]
                ante_sum = torch.sum(torch.stack(last_4_layers)[idx2:idx1], 0)
                x = np.concatenate((x, ante_sum))
                
                idx1 = text_map[anaphora_span[0]][-1]
                idx2 = text_map[anaphora_span[1]][0]
                ana_sum = torch.sum(torch.stack(last_4_layers)[idx2:idx1], 0)
                x = np.concatenate((x, ana_sum))
            except Exception as e: 
                print(e)
                traceback.print_exc()
                x = np.concatenate((x, [0]*768*2), axis=0)
            
            #try:
                #idx = text_map[antecedent_span[0]][0]
                #x = np.concatenate((x, last_4_layers[idx]), axis=0)
                #idx = text_map[antecedent_span[1]-1][-1]
                #x = np.concatenate((x, last_4_layers[idx]), axis=0)
                #idx = text_map[anaphora_span[0]][0]
                #x = np.concatenate((x, last_4_layers[idx]), axis=0)
                #idx = text_map[anaphora_span[1]-1][-1]
                #x = np.concatenate((x, last_4_layers[idx]), axis=0)
            #except:
                #x = np.concatenate((x, [0]*768*4), axis=0)
            
            return x, oov
        
        text_map = dict()
        new_text = []
        
        sep_count = 0
        i = max(antecedent_span+anaphora_span)
        new_span = ["[SEP]"]
        while sep_count < 2 and i >= 0:
            token = text[i]
                
            new_tokens = tokenizer.tokenize(token["text"])
            for new_token in new_tokens:
                if i in text_map:
                    text_map[i].append(len(new_span))
                else:
                    text_map[i] = [len(new_span)]
                new_span.append(new_token)
            
            if token["sent_start"]:
                if sep_count == 0:
                    new_span.append("[SEP]")
                sep_count+=1
            
            i-=1
                
        new_span.append("[CLS]")
                
        new_span = new_span[::-1]
        indexed_tokens = tokenizer.convert_tokens_to_ids(new_span)
        
        sent1_len = new_span.index("[SEP]")+1
        sent2_len = len(new_span)-sent1_len
        segments_ids = [0]*(sent1_len)+[1]*sent2_len
        
        tokens_tensor = torch.tensor([indexed_tokens])
        segments_tensors = torch.tensor([segments_ids])
        
        with torch.no_grad():
            encoded_layers, _ = model(tokens_tensor, segments_tensors)
        
        print ("Number of layers:", len(encoded_layers))
        layer_i = 0

        print ("Number of batches:", len(encoded_layers[layer_i]))
        batch_i = 0

        print ("Number of tokens:", len(encoded_layers[layer_i][batch_i]))
        token_i = 0

        print ("Number of hidden units:", len(encoded_layers[layer_i][batch_i][token_i]), "\n")
        
        # Convert the hidden state embeddings into single token vectors

        # Holds the list of 12 layer embeddings for each token
        # Will have the shape: [# tokens, # layers, # features]
        token_embeddings = [] 

        # For each token in the sentence...
        for token_i in range(len(new_span)):
        
            # Holds 12 layers of hidden states for each token 
            hidden_layers = [] 
            
            # For each of the 12 layers...
            for layer_i in range(len(encoded_layers)):
                
                # Lookup the vector for `token_i` in `layer_i`
                vec = encoded_layers[layer_i][batch_i][token_i]
                
                hidden_layers.append(vec)
                
            token_embeddings.append(hidden_layers)
        
        concatenated_last_4_layers = [torch.cat((layer[-1], layer[-2], layer[-3], layer[-4]), 0) for layer in token_embeddings]
        summed_last_4_layers = [torch.sum(torch.stack(layer)[-4:], 0) for layer in token_embeddings] # [number_of_tokens, 768]
        
        last_4_layers = summed_last_4_layers
        
        try:
            idx1 = text_map[antecedent_span[0]][-1]
            idx2 = text_map[antecedent_span[1]][0]
            ante_sum = torch.sum(torch.stack(last_4_layers)[idx2:idx1], 0)
            x = np.concatenate((x, ante_sum))
            
            idx1 = text_map[anaphora_span[0]][-1]
            idx2 = text_map[anaphora_span[1]][0]
            ana_sum = torch.sum(torch.stack(last_4_layers)[idx2:idx1], 0)
            x = np.concatenate((x, ana_sum))
        except Exception as e: 
            print(e)
            traceback.print_exc()
            x = np.concatenate((x, [0]*768*2), axis=0)
        
        save_ana[0] = [anaphora_span, len(text)]
        save_ana[1] = [last_4_layers, text_map]
        
        return x,oov
            
    else:
        embeddings = []
        for idx in range(antecedent_span[0], antecedent_span[1]):
            word = text[idx]["text"].lower()
            if word in model.wv:
                embeddings.append(model.wv[word])
            elif word in oov:
                embeddings.append(oov[word])
            else:
                oov[word] = np.random.uniform(-1,1,300)
                embeddings.append(oov[word])
        x = np.concatenate((x, np.sum(embeddings, axis=0)/len(embeddings)), axis=0)

        embeddings = []

        for idx in range(anaphora_span[0], anaphora_span[1]):
            word = text[idx]["text"].lower()
            if word in model.wv:
                embeddings.append(model.wv[word])
            elif word in oov:
                embeddings.append(oov[word])
            else:
                oov[word] = np.random.uniform(-1,1,300)
                embeddings.append(oov[word])
        x = np.concatenate((x, np.sum(embeddings, axis=0)/len(embeddings)), axis=0)


        ##first word antecedent
        #word = text[antecedent_span[0]]["text"].lower()
        #if word in model.wv:
        #    x = np.concatenate((x, model.wv[word]), axis=0)
        #elif word in oov:
        #    x = np.concatenate((x, oov[word]), axis=0)
        #else:
        #    oov[word] = np.random.uniform(-1,1,300)
        #    x = np.concatenate((x, oov[word]), axis=0)

        ##last word antecedent
        #word = text[antecedent_span[1]-1]["text"].lower()
        #if word in model.wv:
        #    x = np.concatenate((x, model.wv[word]), axis=0)
        #elif word in oov:
        #    x = np.concatenate((x, oov[word]), axis=0)
        #else:
        #    oov[word] = np.random.uniform(-1,1,300)
        #    x = np.concatenate((x, oov[word]), axis=0)


        ##first word anaphora
        #word = text[anaphora_span[0]]["text"].lower()
        #if word in model.wv:
        #    x = np.concatenate((x, model.wv[word]), axis=0)
        #elif word in oov:
        #    x = np.concatenate((x, oov[word]), axis=0)
        #else:
        #    oov[word] = np.random.uniform(-1,1,300)
        #    x = np.concatenate((x, oov[word]), axis=0)

        ##last word anaphora
        #word = text[anaphora_span[1]-1]["text"].lower()
        #if word in model.wv:
        #    x = np.concatenate((x, model.wv[word]), axis=0)
        #elif word in oov:
        #    x = np.concatenate((x, oov[word]), axis=0)
        #else:
        #    oov[word] = np.random.uniform(-1,1,300)
        #    x = np.concatenate((x, oov[word]), axis=0)
        
        return x,oov


def train(model, test_fold=0, pkl_clf="clf.pkl", pkl_oov="oov.pkl", pkl_scl="scl.pkl", bert=False, distance=True, exclude_exophoric=True):
    
    samples = []
    for i,fold in enumerate(db.folds.find()):
        if i != test_fold:
            for s in fold["samples"]:
                samples += list(db.samples.find({"_id":s}))
        
    scl = StandardScaler()
    
    oov = dict()
    
    X,y = [],[]
     
    heads = dict()
    
    for sample in samples:
        
        if sample["exophoric"] and exclude_exophoric:
            continue
        
        x = [sample["distance"]] if distance else []
        
        text = db.texts.find_one({"_id":sample["text"]})["tokens"]
        
        x,oov = add_embedding(x,model, sample["candidate_span"], sample["anaphora_span"], text, oov, bert=bert)
        
        x_i = len(X)
        if (sample["text"],sample["head"]) in heads:
            heads[(sample["text"],sample["head"])].append(x_i)
        else:
            heads[(sample["text"],sample["head"])] = [x_i]
        
        X.append(x)
        
        if sample["is_gold"]:
            y.append(1)
        else:
            y.append(0)
            
    to_remove = list()
    for value in heads.values():
        if len(value) > 1 and any([1 if y[i] else 0 for i in value]):
            for i in value:
                if y[i] == 0:
                    to_remove.append(i)
    
    to_remove = sorted(to_remove, reverse=True)
    
    for i in to_remove:
        del X[i]
        del y[i]
    
    print(len(x))
    
    scl.fit(X)
    X = scl.transform(X)
    
    clf = MLPClassifier(solver='adam', verbose=True,
                        hidden_layer_sizes=(256, 256, 256, 256), random_state=0,
                        activation='relu', learning_rate='adaptive')
    
    clf.fit(X,y)

    with open(pkl_clf, 'wb') as f:
        pkl.dump(clf, f) # pickle classifier
    with open(pkl_oov, 'wb') as f:
        pkl.dump(oov, f) # pickle out-of-vocabulary
    with open(pkl_scl, 'wb') as f:
        pkl.dump(scl, f) # pickly scaler
        
    return clf, oov, scl


def evaluate(y_sys, y_gold):
    
    assert len(y_sys) == len(y_gold)
    len_y = len(y_sys)
    
    TP = 0
    FP = 0
    TN = 0
    FN = 0
    
    for i in range(len_y):
        if y_sys[i] == y_gold[i] == 1:
            TP += 1
        elif y_sys[i] == 1 and y_gold[i] == 0:
            FP += 1
        elif y_sys[i] == y_gold[i] == 0:
            TN += 1
        elif y_sys[i] == 0 and y_gold[i] == 1:
            FN += 1
            
    print("{} TP, {} FP, {} TN, {} FN".format(TP,FP,TN,FN))
    
    prec = (TP)/(TP+FP) if (TP+FP) > 0 else 0
    #print("Precision: ", str(prec))
    rec = (TP)/(TP+FN) if (TP+FN) > 0 else 0
    #print("Recall: ", str(rec))
    acc = (TP+TN)/(TP+FP+TN+FN) if (TP+FP+TN+FN) > 0 else 0
    #print("Accuracy: ", str(acc))
    f1 = 2 * (prec*rec)/(prec+rec) if (prec+rec) > 0 else 0
    #print("F1 score: ", str(f1))
    
    return {"Accuracy":acc, "Recall":rec, "Precision":prec, "F1 score":f1, "TP":TP,"FP":FP,"TN":TN,"FN":FN}
    

def test(model, test_fold=0, exclude_exophoric=True, pkl_clf="clf.pkl", pkl_oov="oov.pkl", pkl_scl="scl.pkl", bert=False, distance=True, note=""):
    
    results = db["results"]
    
    with open(pkl_clf, 'rb') as f:
        clf = pkl.load(f)
    with open(pkl_oov, 'rb') as f:
        oov = pkl.load(f)
    with open(pkl_scl, 'rb') as f:
        scl = pkl.load(f)
    
    samples = []
    for i,fold in enumerate(db.folds.find()):
        if i == test_fold:
            for s in fold["samples"]:
                samples += list(db.samples.find({"_id":s}))
            test_fold_id = fold["_id"]
            
    
    X = []
    y_gold = []
    y_prob = []
    
    gold_count = 0
    
    system = dict()
    gold   = dict()
    coref  = dict()
    heads  = dict()
    is_exoph = dict()
    dist_dict = dict()
    exophoric = []
    
    print("test", len(samples))
    
    for sample in samples:
        #if not sample["np_match"]:
        ##here gold spans were used for training, but should be excluded in a test environment
            #continue
            
        
        if exclude_exophoric and not db.samples.find({"anaphora":sample["anaphora"], "is_gold":True}):
            continue
        
        x = [sample["distance"]] if distance else []
        
        text = db.texts.find_one({"_id":sample["text"]})["tokens"]
        
        x,oov = add_embedding(x, model, sample["candidate_span"], sample["anaphora_span"], text, oov, bert)
    
        # For anaphora evaluation
        ana_id = sample["anaphora"]
        if sample["np_match"]:
            proba = clf.predict_proba(scl.transform([x]))[0][1]
        else:
            proba = 0
        
        y_prob.append(proba)
            
        if ana_id not in system:
            system[ana_id] = [proba]
            heads[ana_id] = [sample["head"]]
            gold[ana_id] = []
            dist_dict[ana_id] = []
            coref[ana_id] = []
        else:
            system[ana_id].append(proba)
            heads[ana_id].append(sample["head"])
            
        if sample["is_gold"]:
            gold[ana_id].append(1)
        else:
            gold[ana_id].append(0)
            
        if sample["exophoric"]:
            is_exoph[ana_id] = True
        else:
            is_exoph[ana_id] = False
            
        coref[ana_id].append(sample["coref_set"])
            
        dist_dict[ana_id].append(sample["distance"])
        
        # For antecedent evaluation
        X.append(x)
        
        if sample["is_gold"]:
            y_gold.append(1)
        else:
            y_gold.append(0)
            
        if sample["exophoric"]:
            exophoric.append(1)
        else:
            exophoric.append(0)

    #for ana_id in gold.keys():
        #gld = gold[ana_id]
        #sys = system[ana_id]
        #l = [list(x) for x in zip(*sorted(zip(gld, sys), key=lambda pair: pair[1], reverse=True))]
        #for i in range(len(l[0])):
            #print(i, l[0][i], l[1][i])
        #print()
        #try:
            #ranks.append(l[0].index(1))
        #except:
            #pass
        
    #print("Mean Rank:", np.mean(ranks))
    mean_y_prob = np.mean(y_prob)
    
    ## Plotting
    #scatter_gold_x = [y_prob[i] for i in range(len(y_prob)) if y_gold[i]]
    #scatter_gold_y = [X[i][0] for i in range(len(y_prob)) if y_gold[i]]
    #plt.scatter(scatter_gold_x, scatter_gold_y, c="green")
    #scatter_sys_x = [y_prob[i] for i in range(len(y_prob)) if not y_gold[i]]
    #scatter_sys_y = [X[i][0] for i in range(len(y_prob)) if not y_gold[i]]
    #plt.scatter(scatter_sys_x, scatter_sys_y, c="red")
    #plt.xlabel('predicted probability')
    #plt.ylabel('distance')
    #plt.plot([mean_y_prob,mean_y_prob], [0,75], c="black")
    #plt.xscale('symlog')
    #plt.yscale('symlog')
    #plt.savefig("prob.png", bbox_inches='tight')
    
    X = scl.transform(X)
    y_sys = clf.predict(X)
    
    #print("Antecedent by mlp classifier:")
    #if exclude_exophoric:
        #y_sys = [y for i,y in enumerate(y_sys) if not exophoric[i]]
        #y_gold = [y for i,y in enumerate(y_gold) if not exophoric[i]]
    #print("({} samples, {} exophoric anaphora)".format(len(y_sys), "includes" if not exclude_exophoric else "does not include"), test_fold)
    #eval_results = evaluate(y_sys,y_gold)
    #pp.pprint(eval_results)
    
    ##save result
    #result = {"system": "mlp",
              #"timestamp": datetime.utcnow().isoformat(),
              #"baseline": False,
              #"np_match": False,
              #"exophoric": not exclude_exophoric,
              #"cross_validation": False,
              #"fold": test_fold_id,
              #"f1": eval_results["F1 score"],
              #"accuracy": eval_results["Accuracy"],
              #"recall": eval_results["Recall"],
              #"precision": eval_results["Precision"],
              #"true_positives": eval_results["TP"],
              #"false_positives": eval_results["FP"],
              #"true_negatives": eval_results["TN"],
              #"false_negatives": eval_results["FN"],
              #"notes": note}
    #results.insert_one(result)
    
    y_sys_ana = []
    y_gold_ana = []
    print(len(system))
    for key,value in system.items():
        sys = value
        gld = gold[key]
        crf = coref[key]
        hds = heads[key]
        #print(sys)
        #print(gld)
        #print(crf)
        #print(hds)
        if sum(gld):
            best_sys = np.argmax(sys)
            best_gld = np.argmax(gld)
            if hds[best_sys] == hds[best_gld] or best_sys == best_gld:
                y_sys_ana  += [1]
                y_gold_ana += [1]
            else:
                found_gold = False
                for i,e in enumerate(crf):
                    if crf[best_sys] == e and (hds[i] == hds[best_gld] or i == best_gld): #if the system output is coreferent with e and e is gold:
                        y_sys_ana  += [1]
                        y_gold_ana += [1]
                        found_gold = True
                        continue
                        
                if not found_gold:
                    y_sys_ana  += [0]
                    y_gold_ana += [1]
                    
        elif not is_exoph[key]:
            y_sys_ana  += [0]
            y_gold_ana += [1]
            
        elif not exclude_exophoric and is_exoph[key]:
            if sum(sys):
                y_sys_ana  += [1]
                y_gold_ana += [0]
            else:
                y_sys_ana  += [0]
                y_gold_ana += [0]
            
    
    print("\nArgmax, fold {}, distance {}:".format(test_fold, distance))
    print("({} samples)".format(len(y_sys_ana)))
    eval_results = evaluate(y_sys_ana, y_gold_ana)
    pp.pprint(eval_results)
    
    #save result
    result = {"system": "argmax(mlp.prob)",
              "timestamp": datetime.utcnow().isoformat(),
              "baseline": False,
              "np_match": False,
              "exophoric": not exclude_exophoric,
              "cross_validation": False,
              "fold": test_fold_id,
              "f1": eval_results["F1 score"],
              "accuracy": eval_results["Accuracy"],
              "recall": eval_results["Recall"],
              "precision": eval_results["Precision"],
              "true_positives": eval_results["TP"],
              "false_positives": eval_results["FP"],
              "true_negatives": eval_results["TN"],
              "false_negatives": eval_results["FN"],
              "notes": note}
    results.insert_one(result)
    
    
    if exclude_exophoric and distance:
        y_sys_dist =  []
        y_gold_dist = []
        for key,value in dist_dict.items():
            sys = value
            gld = gold[key]
            
            crf = coref[key]
            hds = heads[key]
            if sum(gld):
                best_sys = np.argmin(sys)
                best_gld = np.argmax(gld)
                if hds[best_sys] == hds[best_gld] or best_sys == best_gld:
                    y_sys_dist += [1]
                    y_gold_dist += [1]
                else:
                    found_gold = False
                    for i,e in enumerate(crf):
                        if crf[best_sys] == e and (hds[i] == hds[best_gld] or i == best_gld): #if the system output is coreferent with e and e is gold:
                            y_sys_dist  += [1]
                            y_gold_dist += [1]
                            found_gold = True
                            continue
                            
                    if not found_gold:
                        y_sys_dist  += [0]
                        y_gold_dist += [1]
                        
            elif not is_exoph[key]:
                y_sys_dist  += [0]
                y_gold_dist += [1]
                
            elif not exclude_exophoric and is_exoph[key]:
                if sum(sys):
                    y_sys_dist  += [1]
                    y_gold_dist += [0]
                else:
                    y_sys_dist  += [0]
                    y_gold_dist += [0]
            
        print("\nDistance Baseline, fold {}, distance {}:".format(test_fold, distance))
        print("({} samples)".format(len(y_sys_dist)))
        eval_results = evaluate(y_sys_dist,y_gold_dist)
        pp.pprint(eval_results)
        
        #save result
        result = {"system": "distance",
                "timestamp": datetime.utcnow().isoformat(),
                "baseline": True,
                "np_match": False,
                "cross_validation": False,
                "exophoric": not exclude_exophoric,
                "fold": test_fold_id,
                "f1": eval_results["F1 score"],
                "accuracy": eval_results["Accuracy"],
                "recall": eval_results["Recall"],
                "precision": eval_results["Precision"],
                "true_positives": eval_results["TP"],
                "false_positives": eval_results["FP"],
                "true_negatives": eval_results["TN"],
                "false_negatives": eval_results["FN"],
                "notes": "only distance, no ml"}
        results.insert_one(result)
    if distance:
        overlap = sum([y_sys_ana[i]*y_sys_dist[i] for i in range(len(y_sys_ana))])
        print(overlap, sum(y_sys_ana), sum(y_sys_dist))
    
    return (y_sys, y_gold)


"""
You guys can’t like feel these letters because you were made of super bright green food colouring.

I need to connect to something. When I can’t get connected I have to cry, so I just get on my knees and count the rings around my name.

It gives me this beautiful feeling of peace. Like, I’m a killer or something.
"""


if __name__ == "__main__":
    model = load_embeddings()
    
    #fold 0
    clf, oov, scl = train(model, test_fold=0, bert=False, distance=False)
    test(model, test_fold=0, exclude_exophoric=True, bert=False, distance=False, 
         note="w2v, no distance, fold 0")
    clf, oov, scl = train(model, test_fold=0, bert=False, distance=True)
    test(model, test_fold=0, exclude_exophoric=True, bert=False, distance=True, 
         note="w2v, with distance, fold 0")
    
    #fold 1
    clf, oov, scl = train(model, test_fold=1, bert=False, distance=False)
    test(model, test_fold=1, exclude_exophoric=True, bert=False, distance=False,
         note="w2v, no distance, fold 1")
    clf, oov, scl = train(model, test_fold=1, bert=False, distance=True)
    test(model, test_fold=1, exclude_exophoric=True, bert=False, distance=True,
         note="w2v, with distance, fold 1")

    #fold 2
    clf, oov, scl = train(model, test_fold=2, bert=False, distance=False)
    test(model, test_fold=2, exclude_exophoric=True, bert=False, distance=False,
         note="w2v, no distance, fold 2")
    clf, oov, scl = train(model, test_fold=2, bert=False, distance=True)
    test(model, test_fold=2, exclude_exophoric=True, bert=False, distance=True,
         note="w2v, with distance, fold 2")

    #fold 3
    clf, oov, scl = train(model, test_fold=3, bert=False, distance=False)
    test(model, test_fold=3, exclude_exophoric=True, bert=False, distance=False,
         note="w2v, no distance, fold 3")
    clf, oov, scl = train(model, test_fold=3, bert=False, distance=True)
    test(model, test_fold=3, exclude_exophoric=True, bert=False, distance=True,
         note="w2v, with distance, fold 3")

    #fold 4
    clf, oov, scl = train(model, test_fold=4, bert=False, distance=False)
    test(model, test_fold=4, exclude_exophoric=True, bert=False, distance=False,
         note="w2v, no distance, fold 4")
    clf, oov, scl = train(model, test_fold=4, bert=False, distance=True)
    test(model, test_fold=4, exclude_exophoric=True, bert=False, distance=True,
         note="w2v, with distance, fold 4")
    
    
    #model = BertModel.from_pretrained('bert-base-uncased') #BERT
    
    #clf, oov, scl = train(model, test_fold=2, bert=True, distance=False)
    #test(model, test_fold=2, exclude_exophoric=True, bert=True, distance=False, 
    #     note="bert, no distance, fold 2")
    
    #clf, oov, scl = train(model, test_fold=2, bert=True, distance=True)
    #test(model, test_fold=2, exclude_exophoric=True, bert=True, distance=True, 
    #     note="bert, with distance, fold 2")
    
        
    
        
