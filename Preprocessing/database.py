from pymongo import MongoClient
from mongoengine import *
from bson.objectid import ObjectId
from numpy import argmin
import typing
import spacy
import en_core_web_sm
import secrets
import os
import sys

client = MongoClient('mongodb://localhost:27017')

# texts
class Token(EmbeddedDocument):
    id = ObjectIdField(required=True, default=ObjectId,
                    unique=True, primary_key=True)
    span = ListField(IntField(), max_length=2, required=True)
    text = StringField()
    pos = StringField()
    sent_start = BooleanField(default=False)
    coref_link_forward = ReferenceField('self')
    coref_link_backward = ReferenceField('self')

class Antecedent(EmbeddedDocument):
    id = ObjectIdField(required=True, default=ObjectId,
                    unique=True, primary_key=True)
    span = ListField(IntField(), max_length=2, required=True)
    np_match = BooleanField() # does the span agree with any noun chunk?
    head = IntField()
    coref_set = IntField()
    text = StringField()
    
class Anaphora(EmbeddedDocument):
    id = ObjectIdField(required=True, default=ObjectId,
                    unique=True, primary_key=True)
    span = ListField(IntField(), max_length=2, required=True)
    annotated = BooleanField(default=False) # Set True if annoted or imported.
    antecedent = ReferenceField('Antecedent') # Do not declare if exophoric
    distance = IntField()
    text = StringField()
    
class Text(Document):
    id = ObjectIdField(required=True, default=ObjectId,
                    unique=True, primary_key=True)
    tokens = EmbeddedDocumentListField(Token, required=True)
    candidates = EmbeddedDocumentListField(Antecedent, required=True)
    anaphora = EmbeddedDocumentListField(Anaphora, required=True)
    domain = StringField() #useful for randomisation during cross validation
    source = StringField()
    meta = {'collection': 'texts'}
    
    def process(self, nlp):
        raw_text = []
        for token in self.tokens:
            raw_text.append(token["text"])
        
        doc = nlp.tokenizer.tokens_from_list(raw_text)
        
        for name, proc in nlp.pipeline:
            
            doc.is_parsed=False
            doc=proc(doc)
            doc.is_parsed=True
        
        start_idx = 0
        for sent in doc.sents:
            self.tokens[start_idx]["sent_start"] = True
            start_idx += len(sent)
        
        for chunk in doc.noun_chunks:
            self.candidates.append(Antecedent(span=[chunk.root.left_edge.i,
                                               chunk.root.right_edge.i+1],
                                              text=chunk.text,
                                              head=chunk.root.i,
                                              np_match=True))
        
        


# folds
class Sample(Document):
    id = ObjectIdField(required=True, default=ObjectId,
                    unique=True, primary_key=True)
    anaphora = ReferenceField('Anaphora', required=True)
    anaphora_span = ListField(IntField(), max_length=2, required=True)
    candidate = ReferenceField('Antecedent', required=True)
    candidate_span = ListField(IntField(), max_length=2, required=True)
    np_match = BooleanField()
    distance = IntField()
    is_gold = BooleanField(required=True)
    exophoric = BooleanField()
    head = IntField()
    coref_set = IntField()
    text = ReferenceField('Text', required=True)
    meta = {'collection': 'samples'}

class Fold(Document):
    id = ObjectIdField(required=True, default=ObjectId,
                    unique=True, primary_key=True)
    texts = ListField(ReferenceField('Text'))
    samples = ListField(ReferenceField('Sample'))
    meta = {'collection': 'folds'}


# results
class Result(Document):
    id = ObjectIdField(required=True, default=ObjectId,
                    unique=True, primary_key=True)
    
    system = StringField(required=True)
    timestamp = DateTimeField()
    
    baseline = BooleanField()
    np_match = BooleanField()
    exophoric = BooleanField() #allow exophoric anaphora
    cross_validation = BooleanField() #results after cross_validation
    
    fold = ReferenceField('Fold')
    
    f1 = FloatField()
    accuracy = FloatField()
    recall = FloatField()
    precision = FloatField()
    true_positives = IntField()
    false_positives = IntField()
    true_negatives = IntField()
    false_negatives = IntField()
    
    notes = StringField()
    
    meta = {'collection': 'results'}
    

Token._meta['id_field'] = 'id'
Antecedent._meta['id_field'] = 'id'
Anaphora._meta['id_field'] = 'id'
Text._meta['id_field'] = 'id'
Sample._meta['id_field'] = 'id'
Fold._meta['id_field'] = 'id'
Result._meta['id_field'] = 'id'
    
    

def load_nlp(): #spaCy
    nlp = en_core_web_sm.load()
    nlp.add_pipe(nlp.create_pipe('sentencizer'))
    return nlp
    
def get_client():
    return client

def create_db(overwrite=False) -> None:
    """ 
    Creates empty database.
    
    overwrite:  if False retrieve db or create if not existing
                if True delete db and create new
    """
    if 'ncar' in db and overwrite:
        del client['ncar']
    
    db = client['ncar']
    return db


def update_from_json(db, path_to_file, overwrite=False) -> None:
    """ TODO
    Updates database with texts from json file.
    
    db: texts database
    path_to_file: path object
    overwrite: allow overwrite of already existing texts.
    """
    
    pass

    return db


def update_from_csv(db,path_to_file, overwrite=True) -> None:
    """ TODO: Noun chunking
    Updates database with texts from csv/tsv file.
    
    path_to_file:   path object
    overwrite:      allow overwrite of already existing texts.
    """
    
    #if overwrite == True:
    #    Text.drop_collection()
    
    file_name = path_to_file.split("/")[-1]
    
    text = Text(
        tokens= [],
        candidates= [],
        anaphora = [],
        source = file_name.split(".")[0],
        domain = file_name.split("_")[0] # gets the ectb part of "ectb_1018.ncr"
    )
    
    heads = dict()
    
    with open(path_to_file, 'r') as f:
        f_read = f.read()
        if "\n\n\n" in f_read:
            print("\033[91m[{}] No comparative anaphora found.            ".format(file_name), end="\r")
            pass
        else:
            token_split, annotation_split, coref_split = f_read.strip().split("\n\n")
            
            # TOKENS
            
            print("[{}] Reading tokens.                    ".format(file_name), end="\r")
            token_lines = token_split.strip().split("\n")
            for i,line in enumerate(token_lines):
                line_split = line.split("\t")
                token_str = line_split[-1]
                if token_str[0] == "/" and len(token_str)==2:
                    token_str = token_str[1:]
                text.tokens.append(Token(span=[i,i+1], text=token_str))
            
            #COREFERENCE
            print("[{}] Reading coreference.                ".format(file_name), end="\r")
            coref_lines = coref_split.strip().split("\n")
            coref_sets = []
            for line in coref_lines:
                line_split = line.split("\t")
                coref_sets.append({(int(line_split[i]), int(line_split[i+1])+1) for i in range(0, len(line_split)-1, 2)})
            for line in coref_lines:
                line_split = line.split("\t")
                # last token of first mention links to first token of next mention
                links = [line_split[i:i+2] for i in range(1,len(line_split)-1,2)]
                
                for link in links:
                    # link token objects in both directions
                    # forward
                    text.tokens[int(link[0])].coref_link_forward = text.tokens[int(link[1])].id
                    # backward
                    text.tokens[int(link[1])].coref_link_backward = text.tokens[int(link[0])].id
                    
            # ANNOTATION
            
            print("[{}] Reading anaphoric annotation.                ".format(file_name), end="\r")
            # antecedent parse
            print("[{}] Loading nlp model.                          \033[92m".format(file_name), end="\r")
            nlp = load_nlp()
            text.process(nlp)
            
            # anaphora parse
            annotation_lines = annotation_split.strip().split("\n")
            for i,line in enumerate(annotation_lines):
                line = line.split("\t")
                
                span = [int(line[0]), int(line[1])+1]
                
                text.anaphora.append(Anaphora(
                                        span = span,
                                        text = " ".join([t.text for t in text.tokens[span[0]:span[1]]])
                                        ))
                
                
                if len(line) > 2: #anaphoric
                    antecedent_span = [int(line[2]), int(line[3])+1]
                    antecedent = None
                    
                    for candidate in text.candidates: #find span match
                        if candidate.span == antecedent_span:
                            antecedent = candidate
                    
                    # add gold antecedent with no np match to candidates
                    if antecedent == None:
                        
                        
                        raw_text = [t.text for t in text.tokens[antecedent_span[0]:antecedent_span[1]]]
                        
                        doc = nlp.tokenizer.tokens_from_list(raw_text)
        
                        for name, proc in nlp.pipeline:
            
                            doc.is_parsed=False
                            doc=proc(doc)
                            doc.is_parsed=True
                        
                        head_i = antecedent_span[1]-1
                        for token in doc:
                            if token.dep_ == "ROOT":
                                head_i = antecedent_span[0]+token.i
                        
                        text.candidates.append(Antecedent(
                                                span = antecedent_span,
                                                text = " ".join(raw_text),
                                                np_match = False,
                                                head=head_i
                                                ))
                        antecedent = text.candidates[-1]
                    
                    text.anaphora[-1].antecedent = antecedent.id
                    text.anaphora[-1].distance = span[0] - antecedent_span[1]
                
                
            max_i = len(coref_sets)
            for candidate in text.candidates:
                found_set = False
                for i,coref_set in enumerate(coref_sets):
                    if tuple(candidate.span) in coref_set:
                        candidate.coref_set = i
                        found_set = True
                        break
                if not found_set:
                    candidate.coref_set = max_i
                    max_i += 1
            
            text.save()
            
            return text


def create_from_json(path_to_file, overwrite = False):
    """
    Creates database with texts from json file.
    
    path_to_file:   path object
    overwrite:      allow to create db if there is already a database
                    running on this machine.
    """
    
    db = create_db(overwrite)
    db = update_from_json(path_to_file)
    
    return db

def create_from_csv(path_to_file, overwrite = False) -> None:
    """
    Creates database with texts from csv/tsv file.
    
    path_to_file:   path object
    overwrite:      allow to create db if there is already a database
                    running on this machine.
    """
    
    db = create_db(overwrite)
    db = update_from_csv(path_to_file)
    
    return db
    
    
def update_samples(db,overwrite = True,
                   update_cross_validation = True, fold_count=5, max_distance=75, max_sents = 2) -> None:
    """ TODO Possibility to not overwrite
    Updates samples database from texts database with new annotations. 
    
    samples_db:                 Database to be updated.
    texts_db:                   Database from which to fetch samples.
    overwrite:                  Allow changing of annotations.
    update_cross_validation:    Re-sort already sorted values for cross_validation.
    folds:                      Number of cross validation folds.
    """
    
    folds = [Fold(texts=[],samples=[]) for _ in range(fold_count)]
    text_count = 0
    ana_count = 0
    selected = 0
    disposed = 0
    for txt in db.texts.find(): #text
        print("[db] {} texts processed ({} anaphora, {} candidates).           ".format(text_count, ana_count, selected), end="\r")
        text_count+=1
        ana_count += len(txt["anaphora"])
        
        #extract samples from each text
        target_index = argmin([len(fold.samples) for fold in folds])
        folds[target_index].texts.append(txt["_id"])
        
        for ana in txt["anaphora"]: #anaphora
            #each anaphora is one gold annotation
            for cnd in txt["candidates"]: #candidate
                distance = ana["span"][0] - cnd["span"][-1]
                if max_sents:
                    sent_count = 0
                    span = txt["tokens"][cnd["span"][-1]:ana["span"][0]]
                    # if there are more than two sentences within the span
                    dists = [ana["span"][1] - tok["span"][0] for tok in span if tok["sent_start"]]
                    #print([t["text"] for t in span])
                    if len(dists) > max_sents-1 or distance < 1:
                        disposed += 1
                        continue
                else:
                    if distance > max_distance or distance < 1:
                        disposed += 1
                        continue
                
                selected +=1
                #if distance > 75:
                    #print(distance, ana["span"][0],cnd["span"][-1], len([None for tok in span if tok["sent_start"]]),
                          #" ".join([tok["text"] for tok in span]))
                
                sample = Sample()
                
                sample.distance = ana["span"][0] - cnd["span"][-1]
                sample.anaphora = ana["_id"]
                sample.anaphora_span = ana["span"]
                sample.candidate = cnd["_id"]
                sample.candidate_span = cnd["span"]
                sample.np_match = cnd["np_match"]
                sample.head = cnd["head"]
                sample.coref_set = cnd["coref_set"]
                sample.text = txt["_id"]
                
                if "antecedent" in ana:
                    sample.exophoric = False
                else:
                    sample.exophoric = True
                
                if "antecedent" in ana and ana["antecedent"] == cnd["_id"]:
                    sample.is_gold = True
                else:
                    sample.is_gold = False
                    
                if not cnd["np_match"]:
                    sample.is_gold = True
                    
                sample.save()
                folds[target_index].samples.append(sample["id"])
        
    for i,fold in enumerate(folds):
        
        print("Save fold {}.                      ".format(i), end="\r")
        fold.save()
    return folds



if __name__ == "__main__":
    """
    If called directly, import from file.
    """
    
    db_name = "ncar_engine"
    
    connect(db_name, host='localhost', port=27017)
    
    db = client[db_name]
    client.drop_database(db_name)
    #db.drop_collection("folds")
    #db.drop_collection("samples")
    
    root = "/home/students/zimmermann/Projects/ncar/Preprocessing/corpus/"
    files = os.listdir(root)
        
    for f in files:
        print("[{}] Processing {}.                   ".format(f,f), end="\r")
        update_from_csv(db, root+f)
        print("[{}] Done.                            \033[0m".format(f))
    
    
    print("[db] Update folds.                       ", end="\r")
    update_samples(db, max_sents=2)
    print("[db] Done.                               ")
    
    
    print("Fold stats:")
    count_positives = 0
    count_negatives = 0
    for i,fold in enumerate(db.folds.find()):
        #print(text)
        len_samples = len(fold["samples"])
        positives = len([1 for s in fold["samples"] if db.samples.find_one({"_id":s})["is_gold"]])
        negatives = len_samples - positives
        
        print(i, len_samples, positives, negatives)
        count_positives += positives
        count_negatives += negatives
    print("Total:", count_positives+count_negatives, count_positives, count_negatives)
